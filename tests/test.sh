files=`ls tests/*.um`
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
fail=0
for f in $files;
do
    stripped_file="${f%%.*}"
    out_file=$stripped_file".output"
    in_file=$stripped_file".input"
    expected_out="$(cat $out_file)"
    output="$(./compiler $f scroll && cat $in_file | ./universal-machine scroll)"
    if [ "$expected_out" = "$output" ]
    then
        echo -e "${GREEN}[OK] $NC"$f
    else
        fail=1
        echo -e "$RED[KO] $NC"$f
        echo "got:"
        echo $output
        echo "expected:"
        echo $expected_out
    fi
done;
# for documentation generation
files=`ls tests/*.umr`
for f in $files;
do
    $(./compiler $f scroll)
done

echo "Testing sandmark..."
output="$(./universal-machine tests/sandmark.umz)"
expected="$(cat tests/sandmark-output.txt)"
if [ "$expected" = "$output" ]
then
    echo -e "${GREEN} [OK] Sandmark"
else
    fail=1
    echo -e "$RED[KO] $NCSandmark"
fi
ko="
     )    )
  ( /( ( /(
  )\()))\())
|((_)\((_)\\
|_ ((_) ((_)
| |/ / / _ \\
| ' < | (_) |
|_|\_\ \___/
"
ok="
   OOOOOOOOO     KKKKKKKKK    KKKKKKK
   OO:::::::::OO   K:::::::K    K:::::K
 OO:::::::::::::OO K:::::::K    K:::::K
O:::::::OOO:::::::OK:::::::K   K::::::K
O::::::O   O::::::OKK::::::K  K:::::KKK
O:::::O     O:::::O  K:::::K K:::::K
O:::::O     O:::::O  K::::::K:::::K
O:::::O     O:::::O  K:::::::::::K
O:::::O     O:::::O  K:::::::::::K
O:::::O     O:::::O  K::::::K:::::K
O:::::O     O:::::O  K:::::K K:::::K
O::::::O   O::::::OKK::::::K  K:::::KKK
O:::::::OOO:::::::OK:::::::K   K::::::K
 OO:::::::::::::OO K:::::::K    K:::::K
   OO:::::::::OO   K:::::::K    K:::::K
     OOOOOOOOO     KKKKKKKKK    KKKKKKK
"
if [ $fail -eq 1 ]
then
    echo -e $RED"$ko"
else
    echo -e $GREEN"$ok"
fi

rm scroll