machine:
	jbuilder build um/main.exe --dev
	cp -L _build/default/um/main.exe universal-machine

compiler:
	cp um/bound64.ml sum
	cp um/platter.ml sum
	cp um/helper.ml sum
	jbuilder build sum/main.exe --dev
	cp -L _build/default/sum/main.exe compiler
	
test: machine compiler
	sh tests/test.sh

report:
	(cd doc;\
	bash include_md.sh report.md > full_report.md;\
	./side_code.sh full_report.md;\
	pandoc -t json out.md | python ./tex_templates/underline.py | \
	pandoc -f json -o report.pdf --pdf-engine=xelatex --toc \
	--template=tex_templates/template.latex &&\
	echo "Done.")

install:
	opam install menhir ocamllex 

clean:
	jbuilder clean
	rm -rf universal-machine compiler _build scroll um/_build

.PHONY: compiler machine tests install core ocamlgraph
