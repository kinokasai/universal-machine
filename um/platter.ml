module Int = Base.Int

type platter = Bound64.t
type instr = {opcode: Bound64.t; a: int; b: int; c:int}
type alloc_instr = {capacity: int; idx: Bound64.t}

(* operator is first 4 bits  *)
let get_opcode platter =
  platter
    |> Helper.rshift 28

let get_areg platter =
  448
  |> Bound64.logand platter
  |> Helper.rshift 6
  |> Bound64.to_int

let get_breg platter =
  56
  |> Bound64.logand platter
  |> Helper.rshift 3
  |> Bound64.to_int

let get_creg platter =
  7
  |> Bound64.logand platter
  |> Bound64.to_int

let special_areg platter =
  234881024
  |> Bound64.logand platter
  |> Helper.rshift 25
  |> Bound64.to_int

let get_value platter =
  Bound64.logand 33554431 platter

let encode_op =
  Helper.lshift 28 

let encode_a value =
  Bound64.of_int value
  |> Helper.lshift 6

let encode_b value =
  Bound64.of_int value
  |> Helper.lshift 3

let encode_c value =
  Bound64.of_int value

let string_of_instr instr =
  let op = Bound64.to_string @@ instr.opcode in
  let a = Int.to_string @@ instr.a in
  let b = Int.to_string @@ instr.b in
  let c = Int.to_string @@ instr.c in
  "(Op: " ^ op ^ ", a: " ^ a ^ ", b: " ^ b ^ ", c:" ^ c ^ ")"

let decode_platter platter =
  let opcode = get_opcode platter in
  let a = match opcode with
          | 13 -> special_areg platter   
          | _ -> get_areg platter in
  let b = match opcode with 
          | 13 -> Bound64.to_int @@ get_value platter
          | _ -> get_breg platter in
  let c = get_creg platter in
  {opcode; a; b; c}

let string_of_platter platter =
  let instr = decode_platter platter in
  string_of_instr instr

let encode_platter op a b c =
  let opcode = encode_op op in
  let a = encode_a a in
  let b = encode_b b in
  let c = encode_c c in
  Bound64.(opcode + a + b + c)

let make_cmov_platter ~dest:a ~from:b ~cond:c =
  encode_platter 0 a b c

let make_at_platter ~dest:a ~index:b ~offset:c =
  encode_platter 1 a b c

let make_set_platter ~index:a ~offset:b ~value:c =
  encode_platter 2 a b c

let make_add_platter ~dest:a ~left:b ~right:c =
  encode_platter 3 a b c

let make_mul_platter ~dest:a ~left:b ~right:c =
  encode_platter 4 a b c

let make_div_platter ~dest:a ~left:b ~right:c =
  encode_platter 5 a b c

let make_nand_platter ~dest:a ~left:b ~right:c =
  encode_platter 6 a b c

let make_stop_platter () =
  encode_platter 7 0 0 0

let make_alloc_platter ~capacity:c ~result_idx:b =
  encode_platter 8 0 b c

let make_dealloc_platter ~idx:c =
  encode_platter 9 0 0 c

let make_write_platter ~value:c =
  encode_platter 10 0 0 c

let make_read_platter ~in_reg:c =
  encode_platter 11 0 0 c

let make_load_platter ~idx:b ~pc:c =
  encode_platter 12 0 b c

let make_orthography_platter ~dest:a ~value:value =
  let opcode = encode_op 13 in
  let a = Helper.lshift 25 @@ Bound64.of_int a in
  let mask = 33554431 in (*"0b1111111111111111111111111111"*)
  let value = Bound64.logand mask @@ Bound64.of_int value in
    Bound64.(opcode + a + value)

let make_bogus_loadjmp ~dest:a ~value:value =
  let opcode = encode_op 14 in
  let a = Helper.lshift 25 @@ Bound64.of_int a in
  Bound64.(opcode + value + a)