module T = struct
        include Base.Int
    let dump t = to_string t
    let sexp_of_t t = Base.Sexp.Atom(to_string t)
    let t_of_sexp = (function | Base.Sexp.Atom t -> of_string t | _ -> zero)
    let hash t = Base.Int.hash @@ to_int t
    let hash_fold_t st t = Base.Int.hash_fold_t st @@ to_int t
    let max_32 = 4294967295
    let is_pow2 a = a land (a - 1 ) = 0
    let get_pow2 a = int_of_float @@ log (float_of_int a) /. log 2.0
    let (logand) = (land)
    let bit_and = (land)
    let bit_xor = (lxor) max_32
    let sign_bit = (lxor) min_int
    (* let (lognot) = (lnot) max_32 *)
    let bit_not a = bit_and max_32 @@ (lnot) a
    let bit_nand a b = bit_not @@ bit_and a b
    let to_string = to_string
    let print t = print_endline @@ to_string t
    let print_int t = print_string @@ to_string t
    let plus = (+)
    let (+) a b = bit_and (a + b) max_32
    let ( * ) a b = bit_and (a * b) max_32
    let (/) a b = if is_pow2 b then
                    a lsr (get_pow2 b)
                  else
                    bit_and (a / b)  max_32
end

include(T)
include Core_kernel.Hashable.Make(T)
