open Core
open Helper
open Kaputt.Abbreviations
open Machine
open Platter
open Uint32

let _ = 
    Test.add_simple_test
    ~title:"platter"
    (fun () ->
        Assert.equal (get_areg @@ encode_a 2) 2;
        Assert.equal (get_breg @@ encode_b 2) 2;
        Assert.equal (get_creg @@ encode_c 2) 2;
        Assert.equal (get_opcode @@ encode_op 10) 10;
        let platter = (Uint32.of_string "0b000000000000000000000000001011010") in
        Assert.equal (make_cmov_platter 1 3 2) platter
    )

let _23 = Uint32.of_int 23

let _ =
    Test.add_simple_test
    ~title:"cmov"
    (fun () -> 
        let machine = Machine.init None in
        machine.registers.(1) <- _23;
        cmov machine @@ make_cmov_platter 3 1 1;
        Assert.equal machine.registers.(3) (_23)
    );
    Test.add_simple_test
    ~title:"cmov c = 0"
    (fun () ->
        let machine = Machine.init None in
        machine.registers.(0) <- Uint32.zero;
        machine.registers.(2) <- _23;
        cmov machine @@ make_cmov_platter 2 1 0;
        Assert.equal machine.registers.(2) (_23)
    )

let _ =
    Test.add_simple_test
    ~title:"at"
    (fun () -> 
        let machine = Machine.init None in
        let platter = make_at_platter 3 2 1 in
        Hashtbl.set machine.memory _23 [| Uint32.zero; Uint32.one |];
        machine.registers.(1) <- Uint32.one;
        machine.registers.(2) <- _23;
        at machine platter;
        Assert.equal machine.registers.(3) Uint32.one
    )
let _ =

    Test.add_simple_test
    ~title:"set"
    (fun () -> 
        let machine = Machine.init None in
        let platter = make_set_platter 3 1 3 in
        Hashtbl.set machine.memory _23 [| Uint32.zero; Uint32.one |];
        machine.registers.(1) <- Uint32.one;
        machine.registers.(3) <- _23;
        Machine.set machine platter;
        Assert.equal (Hashtbl.find_exn machine.memory _23).(1) _23
    )

let _ =
    Test.add_simple_test
    ~title:"add"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_add_platter 0 3 2 in
        machine.registers.(3) <- _23;
        machine.registers.(2) <- _23;
        Machine.add machine platter;
        Assert.equal machine.registers.(0) @@ Uint32.of_int 46
    )

let _ =
    Test.add_simple_test
    ~title:"mul"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_mul_platter 0 3 2 in
        machine.registers.(3) <- _23;
        machine.registers.(2) <- _23;
        Machine.mul machine platter;
        Assert.equal machine.registers.(0) @@ Uint32.of_int Pervasives.(23 * 23)
    )

let _ =
    Test.add_simple_test
    ~title:"div"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_div_platter 0 3 2 in
        machine.registers.(3) <- _23;
        machine.registers.(2) <- _23;
        Machine.div machine platter;
        Assert.equal machine.registers.(0) @@ Uint32.of_int 1
    )

let _ =
    Test.add_simple_test
    ~title:"nand"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_nand_platter 0 3 2 in
        machine.registers.(3) <- Uint32.max_int;
        machine.registers.(2) <- Uint32.zero;
        Machine.nand machine platter;
        Assert.equal machine.registers.(0) Uint32.max_int
    )

let _ =
    Test.add_simple_test
    ~title:"alloc"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_alloc_platter 0 1 in
        machine.registers.(0) <- Uint32.of_int 24;
        Machine.alloc machine platter;
        let array = Hashtbl.find_exn machine.memory machine.registers.(1) in
        Assert.equal (Array.length array) 24;
        Assert.equal array.(23) Uint32.zero
    )

let _ =
    Test.add_simple_test
    ~title:"dealloc"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_dealloc_platter ~idx:0 in
        machine.registers.(0) <- _23;
        Hashtbl.set machine.memory _23 [| _23; _23; _23 |];
        Machine.dealloc machine platter;
        Assert.is_none (Hashtbl.find machine.memory _23)
    )

let _ =
    Test.add_simple_test
    ~title:"load"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_load_platter ~idx:0 ~pc:1 in
        machine.registers.(0) <- _23;
        machine.registers.(1) <- Uint32.one;
        Hashtbl.set machine.memory _23 [| Uint32.zero; _23 |];
        Machine.load machine platter;
        let array = Hashtbl.find_exn machine.memory Uint32.zero in
        Assert.equal (array.(Uint32.to_int machine.pc)) _23
    )

let _ =
    Test.add_simple_test
    ~title:"ortography"
    (fun () ->
        let machine = Machine.init None in
        let platter = make_orthography_platter ~dest:7 ~value:123 in
        Machine.orthography machine platter;
        Assert.equal machine.registers.(7) @@ Uint32.of_int 123
    )

let _ = 
    Test.launch_tests ()