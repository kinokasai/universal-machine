open Core

let read_file filename =
    let rec read_program inchan =
        match In_channel.input_binary_int inchan with
        | None -> []
        | Some(i) -> (Stdint.Uint32.to_int @@ Stdint.Uint32.of_int i)::read_program inchan
    in
    List.to_array @@ read_program (In_channel.create filename)

 let _ = 
    try
        Machine.run_machine @@ read_file Sys.argv.(1)
    with
    | Invalid_argument _ -> print_endline "Usage: ./universal-machine filename"