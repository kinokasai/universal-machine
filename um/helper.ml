open Base

let print_string = Stdio.Out_channel.output_string Stdio.stdout
let print_endline = Stdio.print_endline
let rshift n value = Bound64.shift_right_logical value n
let lshift n value = Bound64.shift_left value n
let nth_exn n l = List.nth_exn l n
let array_get n array = Array.get array n
let array_platter_get n array = Array.get array n
let array_set idx value array = Array.set array idx value

let print_list f l =
    print_string "[";
    List.iter l ~f:f;
    print_endline "]"

let print_tuple ffst fsnd t =
    print_string "(";
    print_string @@ ffst @@ fst t;
    print_string "|";
    print_string @@ fsnd @@ snd t;
    print_string ")"

let list_to_string l =
    "[" ^ (List.fold (List.map l ~f:Int.to_string)
    ~init:"" ~f:(fun acc str -> acc ^ str ^ ";")) ^ "]"

let print_map map =
    Map.iteri map
        ~f:(fun ~key:key ~data:value -> Stdio.print_endline
            @@ key ^ ": " ^ list_to_string value)

let unique =
    let counter = ref (-1) in
    fun () -> Int.incr counter;
    "tmp_" ^ Int.to_string !counter

let unique_int, reset =
    let counter = ref (-1) in
    (fun () -> Int.incr counter;
    !counter),
    (fun () -> counter := (-1))