open Platter
open Bound64

module Hashtbl = Base.Hashtbl
module List = Base.List
module Array = Base.Array
module Out_channel = Core_kernel.Out_channel

exception Halt

type idx = Value of Bound64.t | Range of Bound64.t * Bound64.t

exception BadInstruction of string

let memory = Hashtbl.create (module Bound64)
let free_idxs = ref [Range(1, max_int)]
let regs = [| 0; 0; 0; 0; 0; 0; 0; 0; |]

let print_registers () =
  let str = regs
    |> Array.to_list
    |> List.mapi ~f:(fun i r -> string_of_int i ^ " -> " ^ to_string r ^ " | ")
    |> List.fold_left ~init:"" ~f:(fun acc s -> acc ^ s)
  in
  print_endline @@ "(" ^ str ^ ")"

let print_memory () =
  Hashtbl.iteri memory
    ~f:(fun ~key:idx ~data:array ->
      print_endline @@ "M[" ^ to_string idx ^ "]: size " ^ Int.to_string @@ Array.length array)

let get_next_idx () =
  match !free_idxs with
  | [] -> raise Out_of_memory
  | idx::idxs ->
    match idx with
    | Value(idx) -> free_idxs := idxs; idx
    | Range(low, hi) -> free_idxs := Range(Bound64.succ low, hi)::idxs;
                        low

let free_idx idx =
  free_idxs := Value(idx)::!free_idxs

let memget idx offset =
  (* print_string "memget: "; Bound64.print idx; *)
  (Hashtbl.find_exn memory idx).(to_int offset)

let memset idx offset value =
  (Hashtbl.find_exn memory idx).(to_int offset) <- value

let mapset idx value =
  (* print_string "mapset len: "; Caml.print_int @@ Array.length value; print_endline ""; *)
  Hashtbl.set memory ~key:idx ~data:value

let mapget =
  Hashtbl.find_exn memory

let incr finger =
  Int.(finger + 1)

let print_char ch =
  Out_channel.output_char stdout @@ char_of_int @@ to_int ch;
  Out_channel.flush stdout

let rec cycle finger = 
  let array = mapget 0 in
  (* Caml.print_endline @@ Int.to_string @@ Array.length array; *)
  (* print finger; *)
  let platter = array.(finger) in
  let code = get_opcode platter in
  (* print_memory (); *)
  (* print_registers (); *)
  (* print_endline @@ string_of_platter platter; *)
  (* read_line (); *)
  let a, b, c = (get_areg platter, get_breg platter, get_creg platter) in
  (* print_int code; print_string "|"; print_int a; print_string "|"; print_int b; print_string "|";print_int c; print_endline ""; *)
  match code with
  | 0 -> if regs.(c) <> 0 then regs.(a) <- regs.(b); cycle (incr finger)
  | 1 -> regs.(a) <- (memget regs.(b) regs.(c)); cycle @@ incr finger
  | 2 -> memset regs.(a) regs.(b) regs.(c); cycle @@ incr finger
  | 3 -> regs.(a) <- regs.(b) + regs.(c); cycle @@ incr finger
  | 4 -> regs.(a) <- regs.(b) * regs.(c); cycle @@ incr finger
  | 5 -> regs.(a) <- regs.(b) / regs.(c); cycle @@ incr finger
  | 6 -> regs.(a) <- Bound64.bit_nand regs.(b) regs.(c); cycle @@ incr finger
  | 7 -> ()
  | 8 -> let idx = get_next_idx() in 
         (* print_string "Alloc'd in "; print idx; *)
         mapset idx (Array.create ~len:(Bound64.to_int regs.(c)) 0);
         regs.(b) <- idx; cycle @@ incr finger
  | 9 -> mapset regs.(c) [||]; free_idx regs.(c); cycle @@ incr finger
  | 10 -> print_char regs.(c); cycle @@ incr finger
  | 11 -> regs.(c) <- Bound64.of_int @@ int_of_char @@ Caml.input_char Caml.stdin ; cycle @@ incr finger
  | 12 -> if regs.(b) <> 0 then mapset 0 (Array.copy @@ mapget regs.(b)); cycle (to_int regs.(c))
  | 13 -> let a2, value = (special_areg platter, get_value platter) in
          regs.(a2) <- value; cycle @@ incr finger
  | _ -> raise @@ BadInstruction (string_of_platter platter)

let run_machine prog =
  mapset 0 prog;
  cycle 0