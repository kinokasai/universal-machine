---
institute: Université Pierre et Marie Curie
title: "Projet Compilation Avancée"
subtitle: Rapport de projet
author:
- Alexandre Doussot
teacher: Pierre Talbot
course: CA
place: Paris
date: 4 April 2018
papersize: a4
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\newpage

\include tex_templates/code.tex

\include skeleton/intro.md

\newpage

\include universal-machine/machine.md
 
\newpage

\include compiler/compiler.md
\newpage

\include compiler/sum.md
\newpage

\include parsing/parsing.md
\newpage

\include compiler/ir.md
\newpage

\include compiler/liveness.md
\newpage

\include compiler/allocation.md
\newpage

\include compiler/bytecode.md
\newpage

\include skeleton/test.md