# Infrastructure

## Test suite

### Functionment
The test suite is comprised of a small
shell script finding all the programs
finishing by `um`, compiles & runs them,
and matches the expected output stored in
output files. The test also includes the
infamous `sandmark.umz`, found on the IFCP's
website. The shell script is invoked by doing
`make check` on the project's root.

### Output
The output is made of lines comprised of an
`[OK] | [KO]` mention, agremented by the usage
of colors. The tests's state is printed at the
end.

## Report builder

The report is built using Markdown, compiled by pandoc
and a bunch of helper scripts. Most of the code
exposed by this report is extracted directly from the
corresponding files. Indeed, the compiler outputs a file
`filename.ir` whenever it compiles a file `filename`.