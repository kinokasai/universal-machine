# Introduction

The aim of this project is two-fold. First, we propose an OCaml implementation of
the "Universal machine" as described by ICFP. Then, we describe how
we implemented a compiler targeting the universal machine from a simple imperative
language, while providing structure for both.

The report is structured as follow. We first remind how the universal machine
operates as well as its structure. Then, we proceed to detail our implementation,
as well as our data structure choice.

Secondly, we describe the compiler we designed, which implements the following features:

* Integer expressions support
* Logical expressions support
* Relational expressions support
* Variable binding support
* Inputing and printing of characters
* If statements support

However, procedures couldn't be supported due to lack of time. We are confident
that our infrastructure would support such an addition easily.

We then explain in depth the several passes of our compiler: `Parsing`,
`Intermediate Code Generation`, `Liveness analysis`, `Register Allocation`
and `Bytecode Generation`.