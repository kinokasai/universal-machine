# Liveness Analysis

In order to compute register allocation, we must go through liveness analysis.

Two temporaries a and b can fit into the same register, if a and b are never “in 
use” at the same time. Thus, many temporaries can fit in few registers; if they 
don't all fit, the excess temporaries can be kept in memory.

Therefore, the compiler needs to analyze the intermediate-representation program 
to determine which temporaries are in use at the same time. We say a variable is 
live if it holds a value that may be needed in the future, so this analysis is 
called liveness analysis.

Live ranges are computed in the following fashion:

### Get the last usage of a temporary

First, we find the last usage of a temporary - when it dies.
In order to do so, we traverse the program from the end and register
the instruction at which it is used.

### Get the first usage

Then, we traverse yet again the program - from the beginning this time -
to find the first definition of a variable.

### Expand bounds

Finally, when we are in possession of the temporary's liveness bounds
we expand them. The process is simplified because no language construct allows
us to jump backwards.


### Example

On the right is the original program, and the live ranges are represented
on the left.

\side
tmp_0 <- 1;
tmp_1 <- 2;
tmp_2 <- 3;
tmp_3 <- add tmp_1 tmp_2;
tmp_4 <- add tmp_3 tmp_0;
print (tmp_4);
\middle_side
tmp_0 -> [0, 1, 2, 3]
tmp_1 -> [1, 2]
tmp_2 -> [2]
tmp_3 -> [3]
tmp_4 -> [4]
\end_side