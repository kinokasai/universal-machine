# The S-UM Compiler

As OCaml is particularly adapted to write compilers -
its types well match an abstract syntax tree structure as well as
having dedicated tooling.
It's what was used to write the compiler.

The S-UM compiler is comprised of five passes:

* Parsing
* Intermediate Representation Codegen
* Liveness Analysis
* Register Allocation
* Bytecode Generation

![Compiler Passes](compiler/passes.png)

Each output is fed as input to the following pass.

Each example presented in this report - unless told otherwise -
has been compiled through our compiler. Results may have been edited
for readability reasons.

### The first pass

At first, we tried implementing a direct translation from the S-UM language
into bytecode. However, we quickly ran intro a problem:
Compiling `1 + 2` is easy.

However, compiling `1 + (2 + (3 + (4 + (5 + (6 + (7 + (8 + 9)))))))` has proved
to be more troublesome. Indeed, we can't allocate a register for each step of
the operation! While rethinking our approach, we plunged into Andrew Apple's
_Modern Compiler Implementation In ML_ for ideas and techniques. As such,
liveness analysis and register allocation phases and some of the descriptions in this
report are directly based on the
work exposed in the aforementioned book.