# Intermediate Representation

In order to compile down to bytecode, it is imperative
to transform the ast into a more low-level representation.
We propose the folowing architecture.

\side
type exp =
    | Const of int
    | Temp of string
    | Binop of binop * exp * exp
    | Load of exp * exp
    | Alloc of exp
    | Nand of exp * exp
    | CTemp of exp * exp
    | LoadJmp of string
    | RelOffset of int

and stmt =
    (* | Exp of exp *)
    | Move of exp * exp
    | Store of exp * exp * exp
    | Label of string
    | Jump of exp
    (* | CJump of exp * int * int *)
    | Print of exp

\middle_side

and binop =
    | Add | Mul | Div

type instr = {
    instr: stmt;
    src: string list;
    dst: string;
    is_move: bool;
}

and prog = instr list

type addr =
    | Register of int
    | Memory of int
\end_side

During the rest of this report, we'll be using a special layout
to show transformations. Original code will be on the right, and
transformations will be found on the left.

### Binary operations

Simple binary operations are compiled in the following fashion:
Load immediates in a temporary, then add them.

\side
\include ../tests/test_add.um
\middle_side
\include ../tests/test_add.um.ir
\end_side

### Logical operations

While the `not` operator left us stupendous for some time - as we cofused it with a
logical not - we actually found a way to compile it.
Essentially, we set a temporary to 1 and flip it to 0 if the input is true.

\side
\include ../tests/test_not.umr
\middle_side
\include ../tests/test_not.umr.ir
\end_side

\newpage

### Relational operations

The `eq` operator gave us some hard time as well. We knew it was made of a not and a 
xor, but couldn't find the problem. Of course, it all resolved when we fixed the not.

\side
\include ../tests/test_report_eq.umr
\middle_side
\include ../tests/test_report_eq.umr.ir
\end_side

Those are tricky. We have decided to take the following approach. Substract right 
operand from the left one, and check the sign bit.
In our representation, it is the most significant bit.
In order to get, the sign bit, one must shift. However, those are not available,
so we emulate left shifts with divs.
Then again, substraction is not available on this system. To palliate this,
we can flip the sign bit of a number by logical not'ing it, and add 1.
\side
\include ../tests/test_report_lt.umr
\middle_side
\include ../tests/test_report_lt.umr.ir
\end_side

### Print Strings

When a `print("<string>")` is encountered, the string is converted in chars,
and loaded one-by-one.

\side
\include ../tests/test_print_str.um
\middle_side
\include ../tests/test_print_str.um.ir
\end_side

### Let instruction

`let` instructions are implementing using an hashtable.
Every time a new binding is added, the table is supplemented with a binding
from the variable_id to the temporary. It is then used in presence of a variable.

\side
\include ../tests/test_let.um
\middle_side
\include ../tests/test_let.um.ir
\end_side

### If-else control flow

Labels are created at each `if` encountered.
`loadjmp` is a special instruction loading the address of a label in a 
temporary. Its operation is explained in the Codegen section.

\side
if [condition_exp] then {[instrl1]} else {[instrl2]}
\middle_side
tmp_0 <- [condition_exp];
// Load label addresses
tmp_1 <- pc + 5;
tmp_2 <- pc + 7;
// Change jump address if true
tmp_1 <- tmp_2 if tmp_0;
tmp_3 <- 0;
jmp tmp_1;
tmp_4 <- loadjmp else_0;
tmp_5 <- 0;
// Jump to else
jmp tmp_4;
[instrl1]
tmp_7 <- loadjmp endif_0;
tmp_8 <- 0;
// Jump to end
jmp tmp_7;
 -- else_0 -- ;
[instrl2]
 -- endif_0 -- ;
\end_side