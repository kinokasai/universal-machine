
# Bytecode Generation

Thanks to the relative simplicity of our intermediate language,
ir instructions almost map one-to-one to the Universal Machine's instructions.

### Labels

Jump to labels are implemented in a hack-like fashion.
Essentially, bytecode generation in effectuated in two passes.

The problem stems from the expansion done by the bytecode generation.
Indeed, an ir instruction can be expanded to several bytecode instructions.
Thusly, when the loadjmp is encountered, the compiler does not know at which
location the label really is.

To palliate this, we generate a fake bytecode instruction on loadjmp encounter,which opcode is 14, an invalid one.
Then, we store the address of each label in a map.
Finally, after each instruction has been expended, we traverse the code again,
and replace the bogus instruction with a correct move.