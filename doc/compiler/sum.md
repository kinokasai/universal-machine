
# S-UM

S-UM is a simple imperative language. As such, is supports the following
constructs:

* Variable binding - `let`
* String and char printing
* Character reading from stdin
* if-else control flow

All values manipulated are `int`. A special case is made from strings as
they can't be manipulated and are only meant to be used in conjunction of
`print`.

Values can be manipulated using the following operators:

* Arithmetic: `+-*`
* Relational: `<=>`
* Logical: `and or not`

\include compiler/sum_grammar.tex