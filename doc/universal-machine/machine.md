# Universal Machine

## Specification

### Architecture

The universal-machine is a low-level computer with the following 
characteristics:

* 32 bit architecture
* 8 registers
* 32k memory
* 1 character console

### Instructions

Each instruction is encoded in a 4-byte number.
\begin{figure}[h]
\centering
\begin{BVerbatim}
                       A     C
                       |     |
                       vvv   vvv                    
.--------------------------------.
|VUTSRQPONMLKJIHGFEDCBA9876543210|
`--------------------------------'
 ^^^^                      ^^^
 |                         |
 operator number           B
\end{BVerbatim}
\caption{An instruction}
\end{figure}

Instructions are of number 13 and are the following:

0. Conditional Move.
1. Array Index.
2. Array Amendment.
3. Addition.
4. Multiplication.
5. Division.
6. Not-And.
7. Halt.
8. Allocation.
9. Abandonment.
10. Output.
11. Input.
12. Load Program.
13. Orthography.

Orthography is a special operator. Indeed, it allows to load immediates into a
register. As such, it possesses special structure:
\begin{figure}[h]
\centering
\begin{BVerbatim}
     A  
     |  
     vvv
.--------------------------------.
|VUTSRQPONMLKJIHGFEDCBA9876543210|
`--------------------------------'
 ^^^^   ^^^^^^^^^^^^^^^^^^^^^^^^^
 |      |
 |      value
 |
 operator number
\end{BVerbatim}
\caption{The special instruction layout}
\end{figure}

It is important to note that we can only load 25 bit immediates.


## Implementation

The machine is comprised of 3 main modules:

* Bound64
* Platter
* Machine

### Bound64

This module's structure is dependent on the compiling system's architecture.
Indeed, if it's compiled on a 64 bit system, it will extend Core's `Int`,
whereas it will implement Stdint's `Int64` instead.
This is where the machine's functions are implemented, such as the modulo on
standard operations. It allows implementing Core's Hashable interface for use
with hash tables. Due to the particular nature of OCaml, we had to implement the
div function in an unusual way. Indeed, we make use of that function to retrieve
the sign bit of a number. However, contrary to a logical right shift, preserves
the sign bit. Thus, we included a conditional to do a right shift if the divisor
is a power of two.

### Platter

`platter.ml` is where all encoding and decoding functions are implemented.
It also includes some printing fuctions for debugging.
The platters are implemented using `Bound64`.

### Machine

Finally, machine.ml is where all the action happens. It is mainly comprised
of a `cycle` function which loops until an `halt` is found.
While we're using OCaml, the problem was clearly easier to implem to formulate
in an imperative fashion. As such, the memory is represented as a top-level 
hashtable indexing `platter` arrays and indexed by `Bound64`, and the registers as a `Bound64` array.

### The alloc/dealloc problem

One of the tricky things is the dealloc's implementation. Indeed, when 
an array has been deallocated, its index must be available anew.
We resolved this in the following fashion. An index is either a simple
value - e.g. 0 - or a range, in which all indexes are available.
Such indexes are stocked in a list.
Thus, on allocation, we return the head of the list, splitting the range if it's one.
On deallocation, we insert the previously used index at the _head_ of the list, 
ensuring that we use similar indexes.