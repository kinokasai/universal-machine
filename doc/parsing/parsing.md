# Parsing

In order to build an Abstract Syntax Tree to be worked on,
we used the menhir parser generator in combination with
ocamllex. While ocamllex is pretty standard in the industry,
menhir is a recently developed `LR(1)` parser generator.

This step is kept simple as no desugaring is made during this phase.
\lstset{language=Javascript}

We store the result of parsing in the following structure:
\code
\include ../sum/imp_ast.ml
\end_code