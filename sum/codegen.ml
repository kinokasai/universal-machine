open Platter
open IrAst
open Base

let alloc_map_ref = ref (Hashtbl.create (module String))

let platters = ref []

let labels = Hashtbl.create (module String)
let addrs = Hashtbl.create (module Bound64)

let emit platter =
    platters := platter :: !platters

let get_reg i =
    Hashtbl.find_exn !alloc_map_ref i

let get_first l =
    get_reg (List.hd_exn l)

let get_snd l = 
    get_reg (List.nth_exn l 1)

let get_thrd l =
    get_reg (List.nth_exn l 2)

let emit_binop op dst instr =
    let left = get_first instr.src in
    let right = get_snd instr.src in
    let make_platter = match op with
        | Add -> make_add_platter
        | Mul -> make_mul_platter
        | Div -> make_div_platter
    in
    emit @@ make_platter ~dest:dst ~left:left ~right:right

let emit_load dst instr =
    let idx = get_first instr.src in
    let offset = get_snd instr.src in
    emit @@ make_at_platter ~dest:dst ~index:idx ~offset:offset

let emit_alloc dst instr =
    let size = get_first instr.src in
    emit @@ make_alloc_platter ~capacity:size ~result_idx:dst

let emit_loadjmp dst label =
    let label_id = Helper.unique_int () in
    Hashtbl.set labels ~key:label ~data:label_id;
    emit @@ make_bogus_loadjmp ~dest:dst ~value:label_id

let emit_ctemp dst instr =
    let from = get_first instr.src in
    let cond = get_snd instr.src in
    emit @@ make_cmov_platter ~dest:dst ~from:from ~cond:cond

let emit_nand dst instr =
    let left = get_first instr.src in
    let right = get_snd instr.src in
    emit @@ make_nand_platter ~dest:dst ~left:left ~right:right

let emit_move instr src =
    try 
    let dst = get_reg instr.dst in
    match src with
    | Const(i) -> emit @@ make_orthography_platter ~dest:dst ~value:i
    | Binop(op, _, _) -> emit_binop op dst instr
    | Load _ -> emit_load dst instr
    | Alloc _ -> emit_alloc dst instr
    | LoadJmp label -> emit_loadjmp dst label
    | CTemp _ -> emit_ctemp dst instr
    | RelOffset i -> emit @@ make_orthography_platter ~dest:dst ~value:(i + List.length !platters)
    | Nand _ -> emit_nand dst instr
    | Scan -> emit @@ make_read_platter ~in_reg:dst
    | _ -> Caml.exit (-1)
    with
    | Caml.Not_found ->()
        (* Stdio.print_endline @@ "removed useless move:" ^ IrPrinter.print_instr instr *)

let emit_print instr =
    let src = get_first instr.src in
    emit @@ make_write_platter ~value:src

let emit_store instr =
    let src = get_first instr.src in
    let idx = get_snd instr.src in
    let offset = get_thrd instr.src in
    emit @@ make_set_platter ~index:idx ~offset:offset ~value:src

let emit_label label =
    let label_id = Hashtbl.find_exn labels label in
    Hashtbl.set addrs ~key:label_id ~data:(List.length !platters)

let emit_jump instr =
    let prog_idx = get_first instr.src in
    let pc = get_snd instr.src in
    emit @@ make_load_platter ~idx:prog_idx ~pc:pc

let emit_instr instr =
    match instr.instr with
    | Move(_,src) -> emit_move instr src
    | Store _ -> emit_store instr
    | Print _ -> emit_print instr
    | Label str -> emit_label str
    | Jump _ -> emit_jump instr

let rec replace_jumps = function
    | [] -> []
    | op::platters ->
        match get_opcode op with
        | 14 ->
            (* Bound64.print @@ get_value op; *)
            let jump_addr = Hashtbl.find_exn addrs (get_value op) in
                make_orthography_platter ~dest:(special_areg op) ~value:jump_addr
                :: replace_jumps platters
        | _ -> op :: replace_jumps platters

let get_platters alloc_map prog =
    alloc_map_ref := alloc_map;
    Helper.reset();
    List.iter prog ~f:(fun instr -> emit_instr instr);
    emit @@ make_stop_platter ();
    replace_jumps @@ List.rev !platters