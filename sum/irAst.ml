type exp =
    | Const of int
    | Temp of string
    | Binop of binop * exp * exp
    | Load of exp * exp
    | Alloc of exp
    | Nand of exp * exp
    | CTemp of exp * exp
    | LoadJmp of string
    | RelOffset of int
    | Scan

and stmt =
    (* | Exp of exp *)
    | Move of exp * exp
    | Store of exp * exp * exp
    | Label of string
    | Jump of exp
    (* | CJump of exp * int * int *)
    | Print of exp

and binop =
    | Add | Mul | Div

type instr = {
    instr: stmt;
    src: string list;
    dst: string;
    is_move: bool;
}

and prog = instr list

type addr =
    | Register of int
    | Memory of int