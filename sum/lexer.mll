{
  open Parser
  exception SyntaxError of string
}

let digit = ['0'-'9']
let num = digit+
let id = ['a'-'z''_']+
let white_ = [' ' '\t' '\n']+

rule token = parse
  | '+' { PLUS }
  | '/' { DIV }
  | '*' { MUL }
  | ';' { SEMICOLON }
  | '(' { LPAREN }
  | ')' { RPAREN }
  | '{' { LBRACE }
  | '}' { RBRACE }
  | '=' { EQUALS }
  | '>' { GT }
  | '<' { LT }
  | '"'      { read_string (Buffer.create 17) lexbuf }
  | "if" { IF }
  | "then" { THEN }
  | "else" { ELSE }
  | "let" { LET }
  | "print" { PRINT }
  | "scan" { SCAN }
  | "NOT" { NOT }
  | "AND" { AND }
  | "OR" { OR }
  | id as id { IDENT id }
  | num as num { NUM (int_of_string num)}
  | white_ { token lexbuf }
  | eof { EOF }
  | _ { raise @@ SyntaxError ("Syntax Error: " ^ Lexing.lexeme lexbuf)}

and read_string buf =
  parse
  | '"'       { STRING (Buffer.contents buf) }
  | '\\' '/'  { Buffer.add_char buf '/'; read_string buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\'; read_string buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b'; read_string buf lexbuf }
  | '\\' 'f'  { Buffer.add_char buf '\012'; read_string buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n'; read_string buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r'; read_string buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t'; read_string buf lexbuf }
  | [^ '"' '\\']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      read_string buf lexbuf
    }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { raise (SyntaxError ("String is not terminated")) }