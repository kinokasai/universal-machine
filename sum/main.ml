open Core
open Ir
open Allocation
open Lexer

let write platters filename = 
  let file = Out_channel.create filename in
  List.iter ~f:(Out_channel.output_binary_int file) (List.map platters ~f:Bound64.to_int)


let _ =
  (* let prog = ir_of_ast @@ [Print(Add(Int 12, Add(Int 12, Add(Add (Int 12, Int 12), Int 23))));Print(Int(98))] in *)
  (* print_string @@ print_prog prog;
  print_string @@ print_prog @@ allocation prog; *)
  try
  let filename = Sys.argv.(1) in
  let input = In_channel.create filename in
  (* let code = "if 1 then { if 1 then {print(\"send nudes\")} else {} } else {}" in *)
  let filebuf = Lexing.from_channel input in
  let prog = Parser.init Lexer.token filebuf in
  let prog = ir_of_block prog in
  let (prog, alloc) = allocation prog in
  let platters = Codegen.get_platters alloc prog in
  Out_channel.write_all (filename ^ ".ir") ~data:(IrPrinter.print_prog prog);
  write platters Sys.argv.(2)
  with
  | SyntaxError(str) ->
    print_string str |> exit 3
  | Invalid_argument _ -> print_endline "Usage: ./compiler filename out"