open IrAst
open Interference

module Map = Base.Map
module Hashtbl = Core.Hashtbl
module String = Core.String
module Option = Core.Option
module Stack = Core_kernel.Stack

let marks = Hashtbl.create (module String)
let spilled_memory_loc = Hashtbl.create (module String)
let spilled_count = ref 0
let temp_mem = ref ""

exception NoColoring of string

let make_stores temp_src =
    let temp_offset = Helper.unique () in
    let offset = Helper.unique_int () in
    let load_offset = Ir.make_ir_ortho temp_offset offset in
    let store = Ir.make_ir_store temp_src !temp_mem temp_offset in
    Hashtbl.set spilled_memory_loc ~key:temp_src ~data:offset;
    load_offset::store::[]


let make_loads temp_old temp_new =
    let temp_offset = Helper.unique () in
    let offset = Hashtbl.find_exn spilled_memory_loc temp_old in
    let load_offset = Ir.make_ir_ortho temp_offset offset in
    let load = Ir.make_ir_load temp_new !temp_mem temp_offset in
    [load_offset; load]

let rec add_stores prog temp =
    match prog with
    | [] -> []
    | ({src; dst; is_move; _} as prev_instr)::xs ->
        if is_move && dst = temp then
            let stores = make_stores temp in
            prev_instr:: stores@add_stores xs temp
        else if List.mem temp src then
            let temp_new = Helper.unique () in
            let modified_instr = Ir.update_instr temp temp_new prev_instr in
            let loads = make_loads temp temp_new in
            loads@modified_instr::add_stores xs temp
        else
            prev_instr::add_stores xs temp


let copy_graph g =
    let copy = G.create () in
    G.iter_vertex (fun v -> G.add_vertex copy v) g;
    G.iter_edges (fun v1 v2 -> G.add_edge copy v1 v2) g;
    copy

let try_reg graph vert reg =
    G.fold_succ (fun neigh acc ->
        match Hashtbl.find marks neigh with
        | Some(neigh_reg) -> acc && not (neigh_reg = reg)
        | None -> acc && true
        ) graph vert true

let get_color graph vert k = 
    let rec get_color vert k n =
        if n = k then
            None
        else if try_reg graph vert n then
            Some n
        else
            get_color vert k (n+1)
    in
        get_color vert k 0


(* Step 1: Simplify *)

let simplify graph k =
    Hashtbl.clear marks;
    (* We need to clone g *)
    let stack = Stack.create () in
    let g = copy_graph graph in

    let push_and_remove vert =
        Stack.push stack vert;
        G.remove_vertex g vert
    in
    G.iter_vertex (fun vert ->
        if (G.out_degree g vert) < k then
            push_and_remove vert
        else
            push_and_remove vert
        ) g;
    (* Step 2: Select *)
    let rec select stack spilled =
        match Stack.pop stack with
        | None -> spilled
        | Some(v) ->
            match get_color graph v k with
            | Some(reg) -> Hashtbl.set marks ~key:v ~data:reg; select stack spilled
            | None -> (* Rewrite the program to spill the variable *)
                (* print_endline @@ v ^ " is spilling!"; *)
                select stack (v::spilled)
    in
    select stack []

let colorize graph k =
    simplify graph k

(* Helpers *)

let print_marks mark =
    Hashtbl.iteri mark ~f:(fun ~key:node ~data:register ->
        print_endline @@ node ^ " : reg " ^ string_of_int register) 

(* Interference graph *)
let allocation prog =
    let rec allocation prog =
        (* Allocate for memory *)
        (* print_string @@ IrPrinter.print_prog prog; *)
        (* let _ = read_line() in *)
        let live_ranges = Liveness.compute prog in
        (* Helper.print_map live_ranges; *)
        let graph = Interference.make_interference_graph live_ranges in
        (* Dot.output_graph (open_out_bin "intf.dot") graph; *)
        let spilled = colorize graph 8 in
        spilled_count := !spilled_count + List.length spilled;
        if Core.List.is_empty spilled then
            (prog, marks)
        else
            Core.List.fold spilled ~init:prog ~f:(fun prog temp -> add_stores prog temp)
                |> allocation
    in
    Helper.reset();
    temp_mem := Helper.unique();
    let temp_size = Helper.unique () in
    let prog = Ir.make_ir_ortho temp_size 1000::Ir.make_ir_alloc temp_size !temp_mem::prog in
    allocation prog