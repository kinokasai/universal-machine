type op = Add | Mul | Div

type exp =
    | Int of int
    | Binop of op * exp * exp
    | Not of exp
    | Or of exp * exp
    | And of exp * exp
    | Lt of exp * exp
    | Gt of exp * exp
    | Eq of exp * exp
    | String of string
    | Var of string

and instr =
    | Print of exp
    | Scan of string
    | Let of string * exp
    | If of exp * instr list * instr list

and block = instr list