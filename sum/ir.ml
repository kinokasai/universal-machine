open Base
open Imp_ast
open IrAst

exception StringExp

let unique = Helper.unique

let get_if_labels =
    let counter = ref (-1) in
    let next = (fun () -> Int.incr counter; Int.to_string !counter) in
    (fun () -> let id = next() in ("else_" ^ id , "endif_" ^ id))

let prog = ref []

let vartbl = Hashtbl.create (module String)

(* Individual makers *)
let make_ir_load temp idx offset =
    let instr = Move(Temp(temp), Load(Temp(idx), Temp(offset))) in
    {instr; src=[idx; offset]; dst=temp; is_move=true}

let make_ir_store temp idx offset =
    let instr = Store(Temp(temp), Temp(idx), Temp(offset)) in
    {instr; src=[temp; idx; offset]; dst=""; is_move=false}

let make_ir_alloc temp_size idx =
    let instr = Move(Temp(idx), Alloc(Temp(temp_size))) in
    {instr; src=[temp_size]; dst=idx; is_move=true}

let make_ir_ortho temp i =
    let instr = Move(Temp(temp), Const(i)) in
    {instr; src=[]; dst=temp; is_move=true}

let make_ir_print temp =
    let instr = Print(Temp(temp)) in
    {instr; src=[temp]; dst=""; is_move=false}

let make_ir_nand temp left right =
    let instr = Move(Temp(temp), Nand(Temp(left), Temp(right))) in
    {instr; src=[left; right]; dst=temp; is_move=true}

let make_ir_add temp left right =
    let instr = Move(Temp(temp), Binop(Add, Temp(left), Temp(right))) in
    {instr; src=[left; right]; dst=temp; is_move=true}

let make_ir_div temp left right = 
    let instr = Move(Temp(temp), Binop(Div, Temp(left), Temp(right))) in
    {instr; src=[left; right]; dst=temp; is_move=true}

(* Block makers *)
let make_print_char ch =
    let temp = Helper.unique() in
    let load = make_ir_ortho temp (Char.to_int ch) in
    let print = make_ir_print temp in
    [load;print]

let make_print_string str =
    str
    |> String.to_list
    |> List.map ~f:(make_print_char)
    |> List.fold ~init:[] ~f:(fun prog instrs -> prog@instrs)

(* Emitters *)

let emit_move instr =
    prog := instr::!prog;
    instr.dst

let emit_effect instr =
    prog := instr::!prog


(* Traversal *)

let rec ir_of_int i =
    make_ir_int (unique()) i

and make_ir_int temp i =
    let instr = Move(Temp(temp), Const(i)) in
        emit_move {instr; src=[]; dst=temp; is_move=true}


and ir_of_binop op left right =
    let left = ir_of_exp left in
    let right = ir_of_exp right in
    let temp = unique () in
    let op = match op with
            | Imp_ast.Add -> Add
            | Imp_ast.Mul -> Mul
            | Imp_ast.Div -> Div
    in
    let instr = Move(Temp(temp), Binop(op, Temp(left), Temp(right))) in
        emit_move {instr; src=[left; right]; dst=temp; is_move=true}

and make_ir_not temp value =
    let temp_0 = ir_of_int 0 in
    let _ = make_ir_int temp 1 in
    ir_of_cmov temp temp_0 value
    
and ir_of_not e =
    make_ir_not (unique()) (ir_of_exp e)

and make_ir_lnot temp value =
    make_ir_nand temp value value

and ir_of_lnot e =
    emit_move @@ make_ir_lnot (unique()) (ir_of_exp e)

and ir_of_or left right =
    let temp = unique () in
    let left = ir_of_not left in
    let right = ir_of_not right in
    emit_move @@ make_ir_nand temp left right

and ir_of_and left right =
    let temp = unique() in
    let left = ir_of_exp(left) in
    let right = ir_of_exp(right) in
    let result = emit_move @@ make_ir_nand (unique()) left right in
    emit_move @@ make_ir_nand temp result result

    (* [ left NAND ( left NAND right ) ] NAND
[ right NAND ( left NAND right ) ] *)


and make_ir_xor temp left right =
    let result = emit_move @@ make_ir_nand (unique()) left right in
    let left = emit_move @@ make_ir_nand (unique()) left result in
    let right = emit_move @@ make_ir_nand (unique()) right result in
    make_ir_nand temp left right

and ir_of_xor left right =
    let temp = unique () in
    let left = ir_of_exp left in
    let right = ir_of_exp right in
    emit_move @@ make_ir_xor temp left right

and ir_of_eq left right =
    let result = ir_of_xor left right in
    make_ir_not (unique()) result

and make_ir_eq temp left right =
    let result = emit_move @@ make_ir_xor (unique()) left right in
    make_ir_not temp result

and ir_of_lt left right =
    let right = ir_of_lnot right in
    let temp_1 = ir_of_int 1 in
    let result = emit_move @@ make_ir_add (unique()) right temp_1 in
    (* Make addition *)
    let left = ir_of_exp left in
    let result = emit_move @@ make_ir_add (unique()) left result in
    (* Check sign of result *)
    let temp_2pow15 = ir_of_int 32768 in
    let result = emit_move @@ make_ir_div (unique()) result temp_2pow15 in
    let temp_2pow16 = ir_of_int 65536 in
    let result = emit_move @@ make_ir_div (unique()) result temp_2pow16 in
    let temp_1 = ir_of_int 1 in
    make_ir_eq (unique()) result temp_1

and ir_of_gt left right =
    let right = ir_of_lnot right in
    let temp_1 = ir_of_int 1 in
    let result = emit_move @@ make_ir_add (unique()) right temp_1 in
    (* Make addition *)
    let left = ir_of_exp left in
    let result = emit_move @@ make_ir_add (unique()) left result in
    (* Check sign of result *)
    let temp_2pow15 = ir_of_int 32768 in
    let result = emit_move @@ make_ir_div (unique()) result temp_2pow15 in
    let temp_2pow16 = ir_of_int 65536 in
    let result = emit_move @@ make_ir_div (unique()) result temp_2pow16 in
    let temp_0 = ir_of_int 0 in
    make_ir_eq (unique()) result temp_0

and ir_of_exp = function
    | Int(i) -> ir_of_int i
    | Binop(op, left, right) -> ir_of_binop op left right
    | Var(id) -> Hashtbl.find_exn vartbl id
    | Not(e) -> ir_of_not e
    | And(left, right) -> ir_of_and left right
    | Or(left, right) -> ir_of_or left right
    | Eq(left, right) -> ir_of_eq left right
    | Lt(left, right) -> ir_of_lt left right
    | Gt(left, right) -> ir_of_gt left right
    (* String is high level construct, we can't use it here *)
    | String _ -> raise StringExp

and ir_of_cmov temp from cond =
    let instr = Move(Temp(temp), CTemp(Temp(from), Temp(cond))) in
    emit_move {instr; src=[from; cond]; dst=temp; is_move=true}

and ir_of_reloffset i =
    let temp = unique () in
    let instr = Move(Temp(temp), RelOffset(i)) in
    emit_move {instr; src=[]; dst=temp; is_move=true}

(* Instructions *)

and ir_of_print exp =
    match exp with
    | String(str) ->
        let instrl = make_print_string str in
        prog := (List.rev instrl)@(!prog)
    | _ ->
        let temp = ir_of_exp exp in
        emit_effect @@ make_ir_print temp

and ir_of_let id exp =
    let temp = ir_of_exp exp in
    Hashtbl.set vartbl ~key:id ~data:temp

and ir_of_scan id =
    let temp = unique() in
    let instr = Move(Temp(temp), Scan) in
    Hashtbl.set vartbl ~key:id ~data:temp;
    emit_effect {instr; src=[]; dst=temp; is_move=true}

and insert_jmp_tmp temp =
    let temp_0 = ir_of_int 0 in
    let instr = Jump(Temp(temp)) in
    emit_effect @@ {instr; src=[temp_0; temp]; dst=""; is_move=false}

and insert_jmp label =
    let temp = unique() in
    let instr = Move(Temp(temp), LoadJmp(label)) in
    let _ = emit_move @@ {instr; src=[]; dst=temp; is_move=true} in
    insert_jmp_tmp temp

and insert_label label =
    emit_effect @@ {instr=Label(label); src=[]; dst=""; is_move=false}

and ir_of_if cond trueblk falseblk =
    let else_lbl, endif_lbl = get_if_labels () in
    let _ = falseblk in
    let cond = ir_of_exp cond in
    let false_path = ir_of_reloffset 5 in
    let true_path = ir_of_reloffset 7 in
    let branch = ir_of_cmov false_path true_path cond in
    insert_jmp_tmp branch;
    insert_jmp else_lbl;
    List.iter trueblk ~f:ir_of_instr;
    insert_jmp endif_lbl;
    insert_label else_lbl;
    List.iter falseblk ~f:ir_of_instr;
    insert_label endif_lbl

and ir_of_instr = function
    | Imp_ast.Print(exp) -> ir_of_print exp
    | Imp_ast.Scan id -> ir_of_scan id
    | Let(id, exp) -> ir_of_let id exp
    | If(cond, talt, falt) -> ir_of_if cond talt falt

and ir_of_block block =
    List.iter block ~f:ir_of_instr;
    List.rev !prog

(* Updates *)

let rec update_exp old_temp new_temp exp =
    let update = update_exp old_temp new_temp in
    match exp with
    | Temp(temp) when String.equal temp old_temp -> Temp(new_temp)
    | Load(e1, e2) -> Load(update e1, update e2)
    | Binop(_ as op, e1, e2) -> Binop(op, update e1, update e2)
    | Alloc(e) -> Alloc(update e)
    | CTemp(e1, e2) -> CTemp(update e1, update e2)
    | Nand(e1, e2) -> Nand(update e1, update e2)
    | _ as e -> e

let update_stmt old_temp new_temp stmt =
    let update = update_exp old_temp new_temp in
    match stmt with
    | Move(e1, e2) -> Move(update e1, update e2)
    | Store(e1, e2, e3) -> Store(update e1, update e2, update e3)
    | Print(e) -> Print(update e)
    | Jump _ as jmp -> jmp
    | Label _ as label -> label

let update_instr old new_ instr_ =
    let src = Caml.(List.filter (fun reg -> reg != old) instr_.src) in
    let instr = update_stmt old new_ instr_.instr in
    {instr_ with src=new_::src; instr = instr;}
