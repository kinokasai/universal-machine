open Base
open IrAst

exception NotDef of string

let rec alist_fmem l elt =
    match l with
    | [] -> false
    | (id, _)::l -> String.equal id elt || alist_fmem l elt

let rec alist_smap l f =
    match l with
    | [] -> []
    | (a, b)::l -> (a, f b)::alist_smap l f

let print_live_ranges =
   Helper.print_list (Helper.print_tuple (fun x -> x) (Helper.list_to_string))

let rec spawn low high =
    match low with
    | x when x = high -> [high]
    | x -> x::spawn (low+1) high

let spawn range =
    spawn (fst range) (snd range)

let get_def prog id =
    let rec get_def prog id n =
        match prog with
        | [] -> raise @@ NotDef id
        | instr::_ when String.equal instr.dst id -> n
        | _::prog -> get_def prog id (n+1)
    in
        get_def prog id 0


let get_range prog assoc instr instr_num =
    let f assoc src =
        match alist_fmem assoc src with
        | true -> assoc
        | false -> 
            let def = get_def prog src in
            (src, (def, instr_num))::assoc
    in
    List.fold instr.src ~init:assoc ~f:f

let compute_assoc prog =
    snd @@ 
   List.fold_right prog
        ~f:(fun instr (idx, assoc) ->
            (idx-1, get_range prog assoc instr idx))
        ~init:(List.length prog - 2, [])

let get_ranges prog =
    let assoc = compute_assoc prog in
    alist_smap assoc spawn

let compute_live_ranges prog =
    Map.of_alist_exn (module String) (get_ranges prog)

let compute prog =
    compute_live_ranges prog