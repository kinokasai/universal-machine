open IrAst
open Base

exception NotImplemented

let rec useless = ()

and print_op = function
    | Add -> "add"
    | Mul -> "mul"
    | Div -> "div"

and print_const i =
    Int.to_string i

and print_temp str =
    str

and print_binop op left right =
    print_op op ^ " " ^ print_exp left ^ " " ^ print_exp right

and print_exp exp =
    match exp with
    | Const(i) -> print_const i
    | Temp(temp) -> print_temp temp
    | Binop(op, left, right) -> print_binop op left right
    | Load(idx, offset) -> "M[" ^ print_exp idx ^ "|" ^ print_exp offset ^ "]"
    | Alloc(size) -> "alloc(" ^ print_exp size ^ ")"
    | LoadJmp(str) -> "loadjmp " ^ str
    | CTemp(from, cond) -> print_exp from ^ " if " ^ print_exp cond
    | RelOffset(i) -> "pc + " ^ print_const i
    | Nand(left, right) -> print_exp left ^ " (not-and) " ^ print_exp right
    | Scan -> "scan"

and print_move dest src =
    print_exp dest ^ " <- " ^ print_exp src

and print_print exp =
    "print (" ^ print_exp exp ^ ")"

and print_store exp idx offset =
    "M[" ^ print_exp idx ^ "|" ^ print_exp offset ^ "] <- " ^ print_exp exp

and print_instr instr =
    match instr.instr with
    | Move(dest, src) -> print_move dest src
    | Print(exp) -> print_print exp
    | Store(exp, idx, offset) -> print_store exp idx offset
    | Label(string) -> " -- " ^ string ^ " -- "
    | Jump(exp) -> "jmp " ^ print_exp exp

and print_prog prog =
    List.fold prog ~init:"" ~f:(fun acc stmt -> acc ^ print_instr stmt ^ ";\n" )