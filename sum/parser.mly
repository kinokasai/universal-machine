%{
  open Imp_ast
%}

%token SEMICOLON EOF LPAREN RPAREN LBRACE RBRACE
%token PLUS DIV MUL EQUALS IF THEN ELSE NOT AND OR GT LT SCAN
%token PRINT LET
%token <int> NUM
%token <string> STRING IDENT

%start <Imp_ast.block> init
%%

exp:
  | i=NUM { Int(i) }
  | id=IDENT { Var(id) }
  | str=STRING { String(str) }
  | LPAREN e=exp RPAREN { e }
  | NOT operand=exp { Not(operand) }
  | left=exp PLUS right=exp { Binop(Add, left, right) }
  | left=exp MUL right=exp { Binop(Mul, left, right)}
  | left=exp DIV right=exp { Binop(Div, left, right)}
  | left=exp AND right=exp { And(left, right) }
  | left=exp OR right=exp { Or(left, right) }
  | left=exp EQUALS right=exp { Eq(left, right) }
  | left=exp GT right=exp { Gt(left, right) }
  | left=exp LT right=exp { Lt(left, right) }

instr:
  | PRINT exp=exp { Print(exp) }
  | SCAN id=IDENT { Scan(id)}
  | LET id=IDENT EQUALS exp=exp { Let(id, exp) }
  | IF cond=exp THEN LBRACE talt=instr_list RBRACE ELSE LBRACE falt=instr_list RBRACE {If(cond, talt, falt)}

instr_list:
  | instrl = separated_list(SEMICOLON, instr=instr { instr }) {instrl} 


init:
  instrl = instr_list EOF { instrl }