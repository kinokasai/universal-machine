open Graph

module Map = Base.Map
module Node = struct
    type t = string
    let compare = Pervasives.compare
    let hash = Hashtbl.hash
    let equal = (=)
    let default = ""
end

module H = Graph.Imperative.Graph.Concrete(Node)
module G = struct
    include H
    include Coloring.Make(H)
end

module Bfs = Traverse.Bfs(G)

module Dot = Graph.Graphviz.Dot(struct
   include G (* use the graph module from above *)
   let edge_attributes _ = []
   let default_edge_attributes _ = []
   let get_subgraph _ = None
   let vertex_attributes _ = []
   let vertex_name v = v
   let default_vertex_attributes _ = []
  let graph_attributes _ = []
end)

let intf_graph = G.create ()

(* Interference graph *)
let add_edge g v1 v2 =
    if v1 = v2 then 
        G.add_vertex g v1
    else
        G.add_edge g v1 v2

let interfere range_a range_b =
    List.map (fun i -> List.mem i range_b) range_a
        |> List.fold_left (||) false

let test range_a range_b =
    if interfere range_a range_b then
        add_edge intf_graph
    else (fun _ _ -> ())

let make_interference_graph live_ranges =
    G.clear intf_graph;
    Map.iteri live_ranges ~f:(fun ~key:tempa ~data:range_a -> 
        Map.iteri live_ranges ~f:(fun ~key:tempb ~data:range_b ->
            test range_a range_b tempa tempb));
    intf_graph